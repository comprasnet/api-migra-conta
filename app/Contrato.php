<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contrato extends Model
{
    protected $table = 'contrato';

    protected $primaryKey = 'con_id';

    public function lista($id = null)
    {
        $lista = [];
        $contratos = $this->where('con_situacao', 'A')->get();

        if ($id) {
            $contratos = $contratos->find($id);

            if (!$contratos) {
                return $lista;
            }

            $lista[0] = $contratos->toArray();
            $lista[0]['historico'] = $this->buscaHistorico($contratos);
            $lista[0]['responsaveis'] = $this->buscaResponsaveis($contratos);
            $lista[0]['ocorrencias'] = $this->buscaOcorrencias($contratos);
            $lista[0]['terceirizados'] = $this->buscaTerceirizados($contratos);
            $lista[0]['faturas'] = $this->buscaFaturas($contratos);
            $lista[0]['empenhos'] = $this->buscaEmpenhos($contratos);

            return $lista;
        }

        $t = 0;
        foreach ($contratos as $contrato) {
            $lista[$t] = $contrato->toArray();
            $lista[$t]['historico'] = $this->buscaHistorico($contrato);
            $lista[$t]['responsaveis'] = $this->buscaResponsaveis($contrato);
            $lista[$t]['ocorrencias'] = $this->buscaOcorrencias($contrato);
            $lista[$t]['terceirizados'] = $this->buscaTerceirizados($contrato);
            $lista[$t]['faturas'] = $this->buscaFaturas($contrato);
            $lista[$t]['empenhos'] = $this->buscaEmpenhos($contrato);

            $t++;
        }

        return $lista;
    }

    public function getConTipolicitacaoAttribute($value)
    {
        $descricao = $this->buscaCodigoItem($value);
        return $descricao;
    }

    public function getConTipogarantiaAttribute($value)
    {
        $descricao = $this->buscaCodigoItem($value);
        return $descricao;
    }

    public function getConTipocontratoAttribute($value)
    {
        $descricao = $this->buscaCodigoItem($value);
        return $descricao;
    }

    private function buscaCodigoItem($id)
    {
        $retorno = Codigo_item::find($id);
        return $retorno->cit_descricao;
    }

    private function buscaOcorrencias(Contrato $contrato)
    {
        $retorno = [];
        $ocorrencias = $contrato->ocorrencia()->orderBy('oco_num','asc')->get();
        if ($contrato->ocorrencia) {
            foreach ($ocorrencias as $ocorrencia) {
                $retorno[] = $this->montaUrlId('ocorrencia', $ocorrencia->oco_id);
            }
        }

        return $retorno;
    }

    private function buscaFaturas(Contrato $contrato)
    {
        $retorno = [];
        $faturas = $contrato->fatura()->orderBy('fat_ateste','asc')->get();
        if ($contrato->fatura) {
            foreach ($faturas as $fatura) {
                $retorno[] = $this->montaUrlId('fatura', $fatura->fat_id);
            }
        }

        return $retorno;
    }

    private function buscaHistorico(Contrato $contrato)
    {
        $retorno = [];
        $historicos = $contrato->historico()->orderBy('his_data','asc')->get();
        foreach ($historicos as $historico) {
            $retorno[] = $this->montaUrlId('historicocontrato', $historico->his_id);
        }
        return $retorno;
    }

    private function buscaResponsaveis(Contrato $contrato)
    {
        $retorno = [];
        $responsaveis = $contrato->responsaveis()->orderBy('qeq_id','asc')->get();
        foreach ($responsaveis as $responsavel) {
            $retorno[] = $this->montaUrlId('quemequem', $responsavel->qeq_id);
        }
        return $retorno;
    }

    private function buscaTerceirizados(Contrato $contrato)
    {
        $retorno = [];
        foreach ($contrato->terceirizado as $terceirizado) {
            $retorno[] = $this->montaUrlId('terceirizado', $terceirizado->ter_id);
        }
        return $retorno;
    }

    private function buscaEmpenhos(Contrato $contrato)
    {
        $retorno = [];
        foreach ($contrato->empenhos as $empenho) {
            $retorno[] = $this->montaUrlId('empenho', $empenho->emp_id);
        }
        return $retorno;
    }


    public function montaUrlId($table = null, $id = null)
    {
        return url('/api/v1') . '/' . $table . '/' . $id . '?token=' . config('app.key');
    }

    public function historico()
    {
        return $this->hasMany(Historicocontrato::class, 'his_con_id', 'con_id');
    }

    public function responsaveis()
    {
        return $this->hasMany(Quemequem::class, 'qeq_con_id', 'con_id');
    }

    public function ocorrencia()
    {
        return $this->hasMany(Ocorrencia::class, 'oco_connum', 'con_id');
    }

    public function fatura()
    {
        return $this->hasMany(Fatura::class, 'fat_con_id', 'con_id');
    }

    public function terceirizado()
    {
        return $this->hasMany(Terceirizado::class, 'ter_con_id', 'con_id');
    }

    public function empenhos()
    {
        return $this->belongsToMany(Empenho::class, 'empenhoxcontrato', 'exc_con_id', 'exc_empenho');
    }

    public function tipoLicitacao()
    {
        return $this->belongsTo(Codigo_item::class, 'con_tipolicitacao');
    }
}
