<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sec_users extends Model
{
    protected $table = 'sec_users';

    protected $primaryKey = 'login';

    protected $hidden = [
        'pswd',
        'activation_code',
        "priv_admin",
        "passwordsiafi"
    ];
}
