<?php

namespace App\Http\Controllers\Api\V1;

use App\Contratos;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Schema;

class ApiController extends Controller
{
    public function index($table = null, $id = null)
    {
        if ($table) {
            return $this->getTableData($table, $id);
        } else {
            abort(303, 'Favor inserir o nome da tabela na URL.');
        }
    }

    private function getTableData($table, $id)
    {
        $model = '\\App\\' . ucfirst($table);

        if ($table == 'contratos') {
            return $this->getContratos($model);
        }

        if (Schema::hasTable($table)) {

            $model = new $model;

            if ($table == 'contrato') {
                return $this->getContrato($model, $id);
            }

            if ($id) {
                return $model::find($id);
            }

            return $model::all();
        } else {
            abort(303, 'Model [' . $table . '] não encontrado.');
        }
    }

    private function getContrato($model, $id)
    {
        return $model->lista($id);
    }

    private function getContratos()
    {
        $contratos = new Contratos();

        return $contratos->buscarTodos();
    }
}
