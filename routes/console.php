<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('conta:key:generate', function () {
    $this->call('key:generate');

    $APP_KEY = config('app.key');

    // Função especial para verificar se existe sinal de mais (+) no token
    while (strstr($APP_KEY, '/') || strstr($APP_KEY, '+')) {
        $this->error('Chave gerada com caracter especial... gerando nova chave');
        $this->call('key:generate');
        $APP_KEY = config('app.key');
    }

    $this->comment('Favor informar essa CHAVE para importar seus dados!');
    $this->comment(config('app.key'));
})->describe('Cria chave da aplicação para o Conta');

Artisan::command('conta:models:all', function () {
    $tables = [
        'arquivo',
        'arquivo_execucao',
        'arquivo_ocorrencia',
        'arquivo_ted',
        'calendario',
        'cap_areaeixotema',
        'cap_cursos',
        'cap_instituicao',
        'cap_participantes',
        'cap_servidores',
        'cap_unidades',
        'ccxted',
        'cidade',
        'codigo',
        'codigo_item',
        'config',
        'config_ug',
        'contrato',
        'cronograma',
        'crontedfin',
        'docted',
        'emails',
        'empenho',
        'empenhoxcontrato',
        'estado',
        'execucao',
        'fatura',
        'faturaxempenho',
        'fornecedor',
        'historicocontrato',
        'justificativa',
        'maodeobra',
        'menu',
        'ocorrencia',
        'orgao',
        'pi_cc',
        'predio',
        'predio_cc',
        'predio_ug',
        'quemequem',
        'saldoxcontrato',
        'sc_log',
        'sec_apps',
        'sec_groups',
        'sec_groups_apps',
        'sec_logged',
        'sec_sistema_groups',
        'sec_users',
        'sec_users_groups',
        'sec_users_ug',
        'setor',
        'sistema',
        'status',
        'ted',
        'terceirizado',
        'tipolista',
        'ug'
    ];

    foreach ($tables as $t) {
        $className = ucfirst($t);
        $this->comment('Criando Model para a tabela: ' . $className);
        $this->call('make:model', [
            'name' => $className
        ]);
        $this->comment('Atualizando nome da tabela no Model.');
        exec('sed -i "s%//%protected $table = \'' . $t . '\';%g" app/' . $className . '.php');
    }
})->describe('Cria os Models no Laravel a partir do Banco de Dados do Conta');
